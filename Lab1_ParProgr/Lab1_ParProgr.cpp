#include "stdafx.h"
#include <omp.h>
#include <cstdlib>
#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char **argv)
{
	double start, end, res, x;
	double sum = 0;
	const double pi = 3.1415926535897932;
	int i;
	int n = 1;
	while (n > 0) {
		cout << "Enter n" << endl;
		cin >> n;
		double h = 2 * n;
		start = omp_get_wtime();
#pragma omp parallel num_threads(4)
		{
#pragma omp for private (x), reduction(+:sum)
			for (i = 1; i < n; i++) {
				x = (2 * i - 1) / h;
				sum += 4 / (1 + x * x);
			}
		}
		end = omp_get_wtime();
		res = (double)1 / n * sum;
		printf("pi = %.16f error pi = %.16f\n Total running time %f\n", res, abs(pi - res), end - start);
	}
	system("PAUSE");
	return 0;
}